![alt text](https://gitlab.com/cloverowo/libredash/raw/master/banner-transparent-black.png "libredash")

a free and open-source clone of geometry dash for gnu/linux, osx and windows made with LÖVE.
hungry for the bleeding edge? the development branch is [here](https://gitlab.com/yatta/libredash/tree/devel).

## disclaimer
**basically, don't sue me**

libredash has no affiliation with robert topala's "geometry dash". libredash is as much of a "rip-off" of geometry dash as geometry dash is to "the impossible game". libredash shares it's core game design with geometry dash and nothing more. no assets or intellectual property of robtop is used or ever will be used in the development of libredash. i also make no profit from libredash.

## discord server
[![alt text](https://gitlab.com/cloverowo/libredash/raw/master/discord-invite.png "discord")](https://discord.gg/RJygR2c)